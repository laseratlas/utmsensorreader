#include "Urg_driver.h"
#include "Connection_information.h"
#include "math_utilities.h"
#include <iostream>
#include <sstream>
#include <ctime>
#include "ticks.h"
#include "logger.h"
#include "laComBusDriver.h"
#include "taskController.h"
#include "fileOutput.h"
using namespace qrk;
using namespace std;


namespace
{
    void save_data(TaskController * dataSaver, const Urg_driver& urg,
                    const vector<long>& data, long time_stamp)
    {
		ostringstream os;
		os << "Time:" << time_stamp;
#if 0
		// Shows only the front step
        int front_index = urg.step2index(0);
        os << data[front_index] << " [mm], ("
             << time_stamp << " [msec])" << endl;
         Logger::log(os.str(), Logger::DEBUG);
#else
		// Prints the X-Y coordinates for all the measurement points
        long min_distance = urg.min_distance();
        long max_distance = urg.max_distance();
        size_t data_n = data.size();
        for (size_t i = 0; i < data_n; ++i) {
            long l = data[i];
            if ((l <= min_distance) || (l >= max_distance)) {
                continue;
            }

            double radian = urg.index2rad(i);
            long x = static_cast<long>(l * cos(radian));
            long y = static_cast<long>(l * sin(radian));
            os << "(" << x << ", " << y << ")";
        }
        os << endl;
        dataSaver->addNewSensorDataFrame(os.str());
#endif
    }
}


int main(int argc, char *argv[])
{
    Connection_information information(argc, argv);
	
	ostringstream os;
	TaskController taskContrl;
	LaComBusDriver::setMessageParser(&taskContrl);
	taskContrl.setDataBus(LaComBusDriver::getInstance());
	LaComBusDriver::startRxTask();

	// Add data output
	FileOutput logOu(LA_UTM_VE_LOG_FILE);
	Logger::addOutputs(&logOu);
	Logger::addOutputs(LaComBusDriver::getInstance());
	// Start logger thread
	Logger::start();
	
	try {
		// Connects to the sensor
		Urg_driver urg;
		if (!urg.open(information.device_or_ip_name(),
					  information.baudrate_or_port_number(),
					  information.connection_type())) {
			os << "Urg_driver::open(): " << information.device_or_ip_name() << ": " << urg.what() << endl;
			throw runtime_error(os.str());
			os.str("");
		}

		// Gets measurement data
		#if 1
		// Case where the measurement range (start/end steps) is defined
		urg.set_scanning_parameter(urg.deg2step(-90), urg.deg2step(+90), 0);
		#endif
		
		// Configures the PC timestamp into the sensor
		// The timestamp value which comes in the measurement data
		// will match the timestamp value from the PC
		urg.set_sensor_time_stamp(ticks());
    
		// Change controller status to "READY TO START"
		taskContrl.setStatus(LA_DEVICE_STATUS_READY_TO_RUN);
		// Waiting for the start measure CMD
		taskContrl.waitForStartCmd();
		// Put the device in measurement mode
		Logger::log("Putting device into measurement mode...", Logger::INFO);
		Logger::log("Running", Logger::INFO);
		
		urg.start_measurement(Urg_driver::Distance, Urg_driver::Infinity_times, 0);
		while (taskContrl.getStatus() != LA_DEVICE_STATUS_STOPPED) {
			vector<long> data;
			long time_stamp = 0;

			if (!urg.get_distance(data, &time_stamp)) {
				os << "Urg_driver::get_distance(): " << urg.what() << endl;
				throw runtime_error(os.str());
				os.str("");
			}
			save_data(&taskContrl, urg, data, time_stamp);
		}

	#if defined(URG_MSC)
		getchar();
	#endif
	} catch (runtime_error const &err) {
		Logger::log(err.what(), Logger::ERROR);
	} catch (...) {
		Logger::log("An unknown fatal error has occured. Aborting.", Logger::ERROR);
	}
	
	Logger::log("exit.", Logger::INFO);

	LaComBusDriver::stopRxTask();

	Logger::stop();
	Logger::join();
	
    return 0;
}
