#ifndef LOGGER_H
#define LOGGER_H

#include <string>
#include <queue>
#include <list>
#include <ostream>
#include <mutex>
#include <condition_variable>
#include <thread>
#include "output.h"

/**
 * @brief The Logger class support outputing log messages
 * into various outputs, for instance std output or files
 */
class Logger
{
public:
    /**
     * @brief The Type enum define the log message types
     */
    enum Type {NLL, INFO, DEBUG, ERROR, SIZE};
    /**
     * @brief Add a log message
     * @param msg message to be added
     * @param type specify the message type, one of [INFO, DEBUG, ERROR]
     */
    static void log(const std::string& msg, Type type = NLL);
    /**
     * @brief Add an Output for the log messages to be written to
     * @param out Output pointer to be added
     */
    static void addOutputs(const Output* out);
    /**
     * @brief Remove an Output for the log message to be written to
     * @param out pointer to Output object to be removed
     */
    static void removeOutputs(const Output* out);
    /**
     * @brief Start the Logger thread
     */
    static void start();
    /**
     * @brief Stop the Logger thread
     */
    static void stop();
    /**
     * @brief Join the Logger thread to the calling thread
     */
    static void join();

private:
    /**
     * @brief Logger class default constructor
     */
    Logger(){}
    /**
     * @brief Function runs in the Logger thread
     */
    static void run();
    /**
     * @brief Function to get the Logger thread runing status
     * @return The Logger thread running status
     */
    static bool isTaskRunning();
    /**
     * @brief Function to set the Logger thread running status
     * @param status The new Logger thread running status
     */
    static void setTaskRunningStatus(bool status);
    /**
     * @brief get the status of the waited coditions of the logger thread
     * @return is the wait condition satisfied
     */
    static bool isWaitConditionMet();
    /**
     * @brief msg_mutex For synchronizing the datas of the Logger in between threads
     */
    static std::mutex logger_mutex;
    /**
     * @brief msg_fifo_condition wait condition for the Logger thread
     */
    static std::condition_variable logger_condition;
    /**
     * @brief msg_fifo data contrainer for the log messages
     */
    static std::queue<std::string> msg_fifo;
    /**
     * @brief output_lst data container for the outputs
     */
    static std::list<const Output*> output_lst;
    /**
     * @brief logger_task_running_status logger task running status flag
     */
    static bool logger_task_running_status;
    /**
     * @brief log_thead the Logger thread
     */
    static std::thread log_thead;
    /**
     * @brief MSG_TYPE_STR string array to hold the text of different log message types
     */
    static const std::string MSG_TYPE_STR[SIZE];
};

#endif // LOGGER_H
