/*
 * FileOutput.cpp
 *
 *  Created on: Nov 7, 2016
 *      Author: Teng Wu
 */

#include "fileOutput.h"

using namespace std;

FileOutput::FileOutput(const std::string & fileName)
{
    // open the file, if not exist create it
    fs.open(fileName, fstream::out);
}

FileOutput::~FileOutput()
{
    // close the file in destructor
    fs.close();
}

void FileOutput::write(const string &msg) const
{
    // write msg to the file
    fs << msg;
}
