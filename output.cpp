#include "output.h"
#include <iostream>

void Output::write(const std::string &msg) const
{
    // By default, writing the messages to std output
    std::cout << msg;
}
