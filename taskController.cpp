/*
 * TaskController.cpp
 *
 *  Created on: Oct 27, 2016
 *      Author: Teng Wu
 */

#include "taskController.h"
#include <unistd.h>

using namespace std;

TaskController::TaskController():
	dataBus(nullptr),
	status(LA_DEVICE_STATUS_INITIALIZING) {
	dataSaveFile.open(LA_UTM_VE_DATA_FILE, fstream::out);
}

TaskController::~TaskController() {
	if(dataBus != nullptr) {
		dataBus = nullptr;

		dataSaveFile.close();
	}
}

LaserAtlasDeviceStatus TaskController::getStatus() {
	LaserAtlasDeviceStatus flag = LA_DEVICE_STATUS_NULL;

	{
		lock_guard<mutex> lk(bus_mutex);
		flag = status;
	}
	return status;
}

void TaskController::setStatus(LaserAtlasDeviceStatus status) {
	{
		lock_guard<mutex> lk(bus_mutex);
		this->status = status;
	}

//	if(status == LA_DEVICE_STATUS_RUNNING)
//		condition.notify_all();
	sendStatusMessage();
}

const LaComBusDriver* TaskController::getDataBus() const {
	return dataBus;
}

void TaskController::setDataBus(const LaComBusDriver* dataBus) {
	this->dataBus = dataBus;
}

void TaskController::waitForStartCmd() {
	while(getStatus() != LA_DEVICE_STATUS_RUNNING)
		sleep(1);
}

void TaskController::parse(const std::string& message) {
	if(dataBus == nullptr)
			return;

		LaserAtlasMessageHeader head;
		if(!dataBus->extractInfoFromMessage(&head, message))
			return;

		switch(head.subType) {
			case LA_CMD_START:
				setStatus(LA_DEVICE_STATUS_RUNNING);
				break;
			case LA_CMD_STOP:
				setStatus(LA_DEVICE_STATUS_STOPPED);
				break;
			case LA_CMD_QUERY:
				sendStatusMessage();
				break;
		}
}

void TaskController::sendStatusMessage() {
	if(dataBus == nullptr)
		return;
	string tx_msg = dataBus->createMessage(LA_STATUS_MESSAGE, getStatus(), "\n");
	dataBus->sendTxMessage(tx_msg);
}

void TaskController::addNewSensorDataFrame(const std::string& frame) {
	// write msg to the file
	dataSaveFile << frame;
}
