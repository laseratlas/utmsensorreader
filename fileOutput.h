/*
 * FileOutput.h
 *
 *  Created on: Nov 7, 2016
 *      Author: Teng Wu
 */

#ifndef FILEOUTPUT_H_
#define FILEOUTPUT_H_

#include <string>
#include <fstream>
#include "output.h"

/**
 * @brief The FileOutput class can be used to output the
 * log messags into specified file
 */
class FileOutput : public Output
{
public:
    /**
     * @brief default constructor of the class FileOutput
     * @param fileName the name of the output file that the log message will be write to
     */
    explicit FileOutput(const std::string & fileName);
    /**
     * @brife destructor of the class FileOutput
     */
    ~FileOutput();
    /**
     * @brief Write messaege to the output file
     * @param msg the message to be written
     */
    virtual void write(const std::string &msg) const;

private:
    // the file stream used to write the log message to
    mutable std::ofstream fs;
};

#endif /* FILEOUTPUT_H_ */
