TARGET = UTMSensorReader

SRCDIR = /src

include build_rule.mk

CXXFLAGS = -std=c++11 -O2 $(INCLUDES) -I include/cpp
LDLIBS = -lm $(shell if test `echo $(OS) | grep Windows`; then echo "-lwsock32 -lsetupapi"; else if test `uname -s | grep Darwin`; then echo ""; else echo "-lrt"; fi; fi) -L$(SRCDIR) -lurg_cpp

#

all : $(TARGET)

clean :
	$(RM) *.o $(TARGET) *.exe

$(TARGET) : Connection_information.o output.o fileOutput.o logger.o laComBusMessageParser.o laComBusDriver.o taskController.o $(URG_CPP_LIB)

