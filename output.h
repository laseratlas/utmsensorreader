#ifndef OUTPUT_H
#define OUTPUT_H

#include <string>
/**
 * @brief The Output class can be used to output the
 * log messags into the standard output(std::cout)
 */
class Output
{
public:
    /**
     * @brief default constructor of the class Output
     */
    explicit Output() {}
    virtual ~Output() {}
    /**
     * @brief Write messaege to the output
     * @param msg the message to be written
     */
    virtual void write(const std::string & msg) const;
};

#endif // OUTPUT_H
