#ifndef __LASER_ATLAS_CONFIG_H_
#define __LASER_ATLAS_CONFIG_H_

#ifdef __cplusplus
#define EXTERN_C extern "C"
#else
#define EXTERN_C
#endif

#define LA_COM_BUS_RECV_MAX_LEN 512

typedef enum {
    SOURCE_DEVICE = 0,
    DESTINATION_DEVICE,	// 1
    MESSAGE_TYPE, // 2
    MESSAGE_SUB_TYPE, // 3
    MESSAGE_CONTENT, // 4
    MESSAGE_MSG_IDX_SIZE // 5
} LaserAtlasMessageIndex;

typedef enum {
	LA_DEVICE_ID_NULL = 0,
	LA_DEVICE_ID_SCREEN, // 1
	LA_DEVICE_ID_XSENSE, // 2
	LA_DEVICE_ID_UTM_VERTICAL, // 3
	LA_DEVICE_ID_UTM_HORIZONTAL, // 4
	LA_DEVICE_ID_SIZE
} LaserAtlasDeviceID;

typedef enum {
	LA_CMD_NULL = 0,
	LA_CMD_START,
	LA_CMD_STOP,
	LA_CMD_QUERY,
	LA_CMD_ID_SIZE
} LaserAtlasCommandMessageID;

typedef enum {
	LA_DEVICE_STATUS_NULL = 0,
	LA_DEVICE_STATUS_INITIALIZING,
	LA_DEVICE_STATUS_READY_TO_RUN,
	LA_DEVICE_STATUS_RUNNING,
	LA_DEVICE_STATUS_SAVING,
	LA_DEVICE_STATUS_STOPPED,
	LA_STATUS_SIZE
} LaserAtlasDeviceStatus;

typedef enum {
	LA_NULL_MESSAGE = 0,
	LA_CMD_MESSAGE,
	LA_STATUS_MESSAGE,
	LA_LOG_MESSAGE,
	LA_ERROR_MESSAGE,
	LA_MSG_TYPE_SIZE,
} LaserAtlasMessageType;

typedef enum {
    CMD_NULL = 0,
    CMD_START,
    CMD_STOP,
    CMD_SIZE
 } LaserAtlasCommandID;
 
typedef union {
	struct {
		uint8_t source;
		uint8_t destination;
		uint8_t messageType;
		uint8_t subType;
	};
	uint8_t seg[4]; // [0]-sourceDeviceID, [1]-destinationDeviceID, [2]-messageType, [3]-subType
} LaserAtlasMessageHeader;

typedef struct {
	LaserAtlasMessageHeader header;
	char* content;
} LaserAtlasMessage;

// absolute paths for data saving 
#define LA_WLAN_SCANNER_DATA_FILE	"/home/pi/laser_atlas/data/wlanScanFile"
#define LA_XSENS_DATA_FILE			"/home/pi/laser_atlas/data/xsensFile"
#define LA_UTM_VE_DATA_FILE			"/home/pi/laser_atlas/data/utmVeFile"
#define LA_UTM_HO_DATA_FILE			"/home/pi/laser_atlas/data/utmHoFile"
#define LA_MESSAGE_SOCKET_PATH		"/tmp/laComBus"

#define LA_XSENS_LOG_FILE	"/home/pi/laser_atlas/log/.xsens_log"
#define LA_UTM_VE_LOG_FILE	"/home/pi/laser_atlas/log/.utmVertic_log"
#define LA_UTM_HO_LOG_FILE	"/home/pi/laser_atlas/log/.utmHoriz_log"

#endif
