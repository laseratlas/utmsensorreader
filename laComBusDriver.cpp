/*
 * laComBusDriver.cpp
 *
 *  Created on: Oct 27, 2016
 *      Author: Teng Wu
 */

#include "laComBusDriver.h"
#include <sys/socket.h>
#include <unistd.h>
#include <sys/un.h>
#include <fcntl.h>
#include <sstream>
#include <errno.h>
#include <vector>
#include <string.h>
#include "logger.h"

using namespace std;

namespace LAComBusDriverNameSpace {
	const uint8_t myAddress = LA_DEVICE_ID_UTM_VERTICAL;
}

LaComBusDriver* LaComBusDriver::driver = new LaComBusDriver();
thread LaComBusDriver::rxtask;
int LaComBusDriver::bus_identifier = -1;
bool LaComBusDriver::rxTaskRunningStatus = false;
LAComBusMessageParser * LaComBusDriver::parser = nullptr;
mutex LaComBusDriver::rx_mutex;

namespace LaComBusDriverNSpace {
	void split(const std::string &s, char delim, std::vector<std::string> &elems) {
		std::stringstream ss;
		ss.str(s);
		std::string item;
		while (std::getline(ss, item, delim)) {
			elems.push_back(item);
		}
	}
}

void LaComBusDriver::setMessageParser(LAComBusMessageParser* parser) {
	if(LaComBusDriver::parser != nullptr)
		return;
	{
		lock_guard<mutex> lk(rx_mutex);
		LaComBusDriver::parser = parser;
	}
}

LaComBusDriver::LaComBusDriver() {
	try{
		struct sockaddr_un addr;
		addr.sun_family = AF_UNIX;
		strncpy(addr.sun_path, LA_MESSAGE_SOCKET_PATH, sizeof(addr.sun_path) - 1);

		int s = socket(AF_UNIX, SOCK_STREAM, 0);
		if(s == -1)
			throw runtime_error("create socket failed, "+to_string(errno));

		bus_identifier = s;

		if(connect(bus_identifier, (__CONST_SOCKADDR_ARG)&addr, sizeof(addr)) == -1)
			throw runtime_error("Socket connect failed, "+to_string(errno));

		// Make socket nonblocking
		int flags = fcntl(bus_identifier, F_GETFL, 0);

		if(flags == -1)
			throw runtime_error("Socket connect failed, "+to_string(errno));

		flags |= O_NONBLOCK;

		if(fcntl(bus_identifier, F_SETFL, flags) == -1) {
			throw runtime_error("fcntl(), F_SETFL, "+to_string(errno));
		}
	} catch(std::runtime_error & err) {
		Logger::log(err.what(), Logger::ERROR);
		if(bus_identifier != -1) {
			close(bus_identifier);
			bus_identifier = -1;
		}
	}
}

LaComBusDriver::~LaComBusDriver() {
	if(bus_identifier != -1) {
		close(bus_identifier);
		bus_identifier = -1;
	}
}

void LaComBusDriver::write(const std::string& msg) const {
	std::string message = this->createMessage(LA_LOG_MESSAGE, Logger::INFO, msg);
	this->sendTxMessage(message);
}

string LaComBusDriver::createMessage(LaserAtlasMessageType type, uint8_t subType, const std::string& content) const {
	LaserAtlasMessageHeader header;
	header.source = LAComBusDriverNameSpace::myAddress;
	header.destination = LA_DEVICE_ID_SCREEN;
	header.messageType = type;
	header.subType = subType;

	ostringstream os;
	os << to_string(header.source) << ":" << to_string(header.destination) << ":" << to_string(header.messageType) << ":" << to_string(header.subType) << ":" << content;
	return os.str();
}

bool LaComBusDriver::extractInfoFromMessage(LaserAtlasMessageHeader *info, const std::string& message) const {
	if(info == nullptr)
		return false;

	// Split the string by ':'
	vector<string> strVec;
	LaComBusDriverNSpace::split(message, ':', strVec);

	if(strVec.size() != MESSAGE_MSG_IDX_SIZE)
		return false;
	// 0
	int source = stoi(strVec[SOURCE_DEVICE]);
	if(source != LA_DEVICE_ID_SCREEN) return false;
	// 1
	int destination = stoi(strVec[DESTINATION_DEVICE]);
	if(destination != LA_DEVICE_ID_NULL) return false;
	// 2
	int type = stoi(strVec[MESSAGE_TYPE]);
	if(type != LA_CMD_MESSAGE) return false;
	// 3
	int subType = stoi(strVec[MESSAGE_SUB_TYPE]);
	if(subType >= LA_CMD_ID_SIZE) return false;

	info->source = source;
	info->destination = destination;
	info->messageType = type;
	info->subType = subType;

	return true;
}

bool LaComBusDriver::isRxTaskRunning() {
	bool flag = false;
	{
		lock_guard<mutex> lk(rx_mutex);
		flag = rxTaskRunningStatus;
	}
	return flag;
}

void LaComBusDriver::startRxTask() {
	if(isRxTaskRunning())
		return;
	setRxTaskRunningStatus(true);

	rxtask = thread(run);
}

void LaComBusDriver::stopRxTask() {
	if(!isRxTaskRunning())
		return;
	setRxTaskRunningStatus(false);
}

void LaComBusDriver::setRxTaskRunningStatus(bool status) {
	{
		lock_guard<mutex> lk(rx_mutex);
		rxTaskRunningStatus = status;
	}
}

void LaComBusDriver::sendTxMessage(const std::string& msg) const {
	int bus_id = bus_identifier;
	if(bus_id == -1)
		return;

	int write_n = ::write(bus_id, msg.c_str(), msg.size());

	if(write_n == -1) {
		if(errno != EAGAIN)
			Logger::log("Mesage send failed", Logger::ERROR);
	}
}

LaComBusDriver* LaComBusDriver::getInstance() {
	return driver;
}

void LaComBusDriver::run() {
	char message[LA_COM_BUS_RECV_MAX_LEN];
	int bus_id = bus_identifier;

	if(bus_id == -1)
		return;

	if(parser == nullptr) {
		Logger::log("Mesage parser is NULL", Logger::ERROR);
		return;
	}

	Logger::log("LAComBusDriver RX task start", Logger::DEBUG);

	while(isRxTaskRunning()) {
		memset((void*)message, 0, sizeof(message));
		int read_n = ::read(bus_id, message, LA_COM_BUS_RECV_MAX_LEN);

		if(read_n == -1) {
			if(errno == EAGAIN)
				sleep(1);
			else
				Logger::log("COMBus read failed", Logger::ERROR);
		}
		else
			parser->parse(string(message));
	}
}
