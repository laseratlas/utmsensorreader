#include <sstream>
#include "logger.h"

using namespace std;

mutex Logger::logger_mutex;
condition_variable Logger::logger_condition;

queue<string> Logger::msg_fifo;
list<const Output*> Logger::output_lst;

bool Logger::logger_task_running_status = false;

thread Logger::log_thead;

const string Logger::MSG_TYPE_STR[Logger::SIZE] = {"", "INFO:", "DEBUG:", "ERROR:"};

void Logger::log(const string & msg, Type type)
{
    ostringstream os("");

    os << MSG_TYPE_STR[(type < Logger::SIZE) ? type : INFO] << msg << endl;

    {
        lock_guard<mutex> lk(logger_mutex);
        msg_fifo.push(os.str());
    }

    logger_condition.notify_all();
}

void Logger::addOutputs(const Output* oput)
{
    {
        lock_guard<mutex> lk(logger_mutex);
        output_lst.push_back(oput);
    }

    #ifdef __DEBUG__
    log("Logger add output", DEBUG);
    #endif

    logger_condition.notify_all();
}

void Logger::removeOutputs(const Output* out)
{
    {
        lock_guard<mutex> lk(logger_mutex);
        output_lst.remove(out);
    }
    #ifdef __DEBUG__
    log("Logger remove output", DEBUG);
    #endif
}

void Logger::start()
{
    // return if Logger thread is already running
    if(isTaskRunning())
        return;

    // set running status flag to True before start the Logger thread
    setTaskRunningStatus(true);
    // start the Logger thread to run the function run()
    log_thead = thread(run);
}

void Logger::stop()
{
    // return if Logger thread is already required to stop
    if(!isTaskRunning())
        return;

    #ifdef __DEBUG__
    log("Logger thread is about to quit", DEBUG);
    #endif
    // set running status flag to False and
    // and notify the Logger thread
    setTaskRunningStatus(false);
}

void Logger::join()
{
    if(log_thead.joinable())
        log_thead.join();
}

bool Logger::isTaskRunning()
{
    bool flag = false;

    {
        lock_guard<mutex> lk(logger_mutex);
        flag = logger_task_running_status;
    }

    return flag;
}

void Logger::setTaskRunningStatus(bool status)
{
    {
        lock_guard<mutex> lk(logger_mutex);
        logger_task_running_status = status;
    }

    // notified the waiting thread if Logger thread is required to quit
    if(!status)
        logger_condition.notify_all();
}

bool Logger::isWaitConditionMet()
{
    // wake up the thread if task is required to stop OR
    // if msg_fifo and output_lst are both nonempty
    return (!logger_task_running_status || (!msg_fifo.empty() && !output_lst.empty()));
}

void Logger::run()
{
    string msg;

    #ifdef __DEBUG__
    log("Logger thread start", DEBUG);
    #endif

    unique_lock<mutex> lk(logger_mutex);

    while(1)
    {
        // If condition is not met then wait to yield cpu time
        logger_condition.wait(lk, isWaitConditionMet);
        // quit the thread if required
        if(!logger_task_running_status)
        {
            lk.unlock();
            return;
        }

        while(!msg_fifo.empty())
        {
            msg = msg_fifo.front();
            msg_fifo.pop();

            for(auto &out: output_lst)
            {
                lk.unlock();
                out->write(msg);
                lk.lock();
            }
        }
    }
    lk.unlock();
}
