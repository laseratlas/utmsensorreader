/*
 * LAComBusMessageParser.h
 *
 *  Created on: Oct 30, 2016
 *      Author: Teng Wu
 */

#ifndef LACOMBUSMESSAGEPARSER_H_
#define LACOMBUSMESSAGEPARSER_H_
#include <string>

class LAComBusMessageParser {
public:
	LAComBusMessageParser() {}
	virtual ~LAComBusMessageParser() {}
	virtual void parse(const std::string & message) {}
};

#endif /* LACOMBUSMESSAGEPARSER_H_ */
